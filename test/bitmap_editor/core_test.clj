(ns bitmap-editor.core-test
  (:require [clojure.test :refer :all]
            [bitmap-editor.core :refer :all]
            [bitmap-editor.bitmap :refer :all]))


(deftest bitmap-editor
  (testing "Create image with correct default colour"
    (is (= (image 5 6)
           [["O" "O" "O" "O" "O"] ["O" "O" "O" "O" "O"] ["O" "O" "O" "O" "O"] ["O" "O" "O" "O" "O"] ["O" "O" "O" "O" "O"] ["O" "O" "O" "O" "O"]]))))
      

(deftest clear-image
  (testing "Clearing an image sets all pixels to 'O'"
    (let [canvas [["C" "F" "G"] ["C" "F" "G"] ["W" "W" "W"]]]
       (is (= (clear canvas) (image 3 3))))))


(deftest moving-around
  (testing "Get the pixel value"
    (let [img [["A" "B" "C"] ["D" "E" "F"] ["G" "H" "I"]]]
      (is (= (pixel img 2 3) "H"))))

  (testing "Get the neighbourhood"
    (let [img [["A" "B" "C"] ["D" "E" "F"] ["G" "H" "I"]]]
      (is (= (into {} (neighbouring-pixels img 2 2))
             (into {} '([[2 1] "B"] [[1 2] "D"] [[3 2] "F"] [[2 3] "H"])))))))


(deftest paint-image
  (testing "Painting a pixel"
    (let [img (image 5 6)]
      (is (= (pixel (paint img 2 3 "A") 2 3) "A"))))

  (testing "Painting a vertical segment"
    (let [img (vertical-paint (image 5 6) 2 3 4 "W")]
      (is (= (pixel img 2 2) "O"))
      (is (= (pixel img 2 3) "W"))
      (is (= (pixel img 2 4) "W"))
      (is (= (pixel img 2 5) "O"))
      (is (= (pixel img 3 3) "O"))))

  (testing "Painting a horizontal segment"
    (let [img (horizontal-paint (image 5 6) 3 4 2 "Z")]
      (is (= (pixel img 2 2) "O"))
      (is (= (pixel img 3 1) "O"))
      (is (= (pixel img 3 2) "Z"))
      (is (= (pixel img 3 3) "O"))
      (is (= (pixel img 4 2) "Z"))
      (is (= (pixel img 5 2) "O"))))

  (testing "Filling an area with colour"
    (let [img      (-> (image 5 5)
                       (vertical-paint 2 3 4 "W")
                       (horizontal-paint 3 4 2 "Z")
                       (paint 1 1 "A")
                       (paint 4 3 "K")
                       (paint 3 4 "M"))
          expected [["A" "X" "X" "X" "X"]
                    ["X" "X" "Z" "Z" "X"]
                    ["X" "W" "O" "K" "X"]
                    ["X" "W" "M" "X" "X"]
                    ["X" "X" "X" "X" "X"]]]
      (is (= expected (fill img 5 5 "X"))))))


(deftest show-image
  (testing "Show the image"
    (let [img (-> (image 5 5)
                  (vertical-paint 2 3 4 "W")
                  (horizontal-paint 3 4 2 "Z")
                  (paint 1 1 "A"))]
      (is (= "AOOOO\nOOZZO\nOWOOO\nOWOOO\nOOOOO"
             (show img))))))


