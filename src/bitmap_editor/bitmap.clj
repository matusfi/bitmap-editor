(ns bitmap-editor.bitmap
  (:require [bitmap-editor.validations :as v]
            [clojure.string :as str]
            [clojure.set :as set]))


(defn image
  "Create a new blank image with given dimensions"
  [cols rows]

  {:pre [(v/positive-integer? cols)
         (v/positive-integer? rows)]} 

  (->> "O"
       (repeat cols)
       vec
       (repeat rows)
       vec))



(defn dimensions
  "Return the dimensions of the image {:cols M :rows N}"
  [img]
  {:pre [(v/canvas-ready? img)]}

  {:cols (count (first img))
   :rows (count img)})



(defn show
  "Print the image in a nice way"
  [img]
  {:pre [(v/canvas-ready? img)]}

  (->> img
       (map #(apply str %))
       (str/join "\n")))



(defn pixel
  "Returns the value of a pixel in an image"
  [img x y]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x) (v/x-in-bounds? img x)
         (v/positive-integer? y) (v/y-in-bounds? img y)]}    

  (get-in img [(dec y) (dec x)]))



(defn clear
  "Clear the current image by creating a brand new image with the same dimensions"
  [img]
  {:pre [(v/canvas-ready? img)]}

  (let [{cols :cols rows :rows} (dimensions img)]
    (image cols rows)))



(defn paint
  "Paint a pixel with a colour"
  [img x y colour]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x) (v/x-in-bounds? img x)
         (v/positive-integer? y) (v/y-in-bounds? img y)
         (v/colour? colour)]}
  
  (assoc-in img [(dec y) (dec x)] colour))




(defn vertical-paint
  "Paint a vertical segment in a column between specified range of rows"
  [img x y-start y-end colour]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x)       (v/x-in-bounds? img x)
         (v/positive-integer? y-start) (v/y-in-bounds? img y-start)
         (v/positive-integer? y-end)   (v/y-in-bounds? img y-end)
         (< y-start y-end)
         (v/colour? colour)]}
  
  (reduce (fn [acc y] (paint acc x y colour))
          img
          (range y-start (inc y-end))))




(defn horizontal-paint
  "Paint a horizontal segment in a row delimited by a range of columns"
  [img x-start x-end y colour]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x-start) (v/x-in-bounds? img x-start)
         (v/positive-integer? x-end)   (v/x-in-bounds? img x-end)
         (v/positive-integer? y)       (v/y-in-bounds? img y)
         (< x-start x-end)
         (v/colour? colour)]}
  
  (reduce (fn [acc x] (paint acc x y colour))
          img
          (range x-start (inc x-end))))




(defn neighbouring-pixels
  "Return a list of neighbouring pixels"
  [img x y]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x) (v/x-in-bounds? img x)
         (v/positive-integer? y) (v/y-in-bounds? img y)]}
 
  (let [{cols :cols
         rows :rows} (dimensions img)
        up           [x (if (> y 1) (dec y) y)]
        down         [x (if (< y rows) (inc y) y)]
        left         [(if (< x cols) (inc x) x) y]
        right        [(if (> x 1) (dec x) x) y]
        neighbours   (set/difference (set [up down left right]) #{[x y]})]
    (map (fn [[x y]] [[x y] (pixel img x y)])
         neighbours)))





(defn fill
  "Fill an area of the same colour with another colour"
  [img x y colour]

  {:pre [(v/canvas-ready? img)
         (v/positive-integer? x) (v/x-in-bounds? img x)
         (v/positive-integer? y) (v/y-in-bounds? img y)
         (v/colour? colour)]}
  
  (loop [points-to-fill [[[x y] (pixel img x y)]]
         step           img]
    (if (empty? points-to-fill)
      step
      (let [[[x y] clr] (peek points-to-fill)
            neighbours  (filter
                         #(= clr (second %))
                         (neighbouring-pixels step x y))]
        (recur (apply conj
                      (pop points-to-fill)
                      neighbours)
               (paint step x y colour))))))
