(ns bitmap-editor.core
  (:require [clojure.string :as str]
            [bitmap-editor.bitmap :refer :all])
  (:gen-class))
  
(defn prompt []
  (print "✒ ")
  (flush)
  (read-line))


(defn error [& message]
  (binding [*out* *err*]
    (println (apply str message))))


(defn process-line [line]
  (let [elems (-> line
                  str/trim
                  str/upper-case
                  (str/split #" "))]
    (map #(let [arg (read-string %)]
            (if (number? arg) arg (str arg)))
         (take 5 elems))))


(defn -main
  [& args]
  (loop [line   (prompt)
         canvas (atom [])]
    (let [[cmd a1 a2 a3 a4]   (process-line line)] 
      (try
        (case cmd
          "I" (reset! canvas (image a1 a2))
          "C" (swap! canvas clear)
          "L" (swap! canvas paint a1 a2 a3)
          "V" (swap! canvas vertical-paint a1 a2 a3 a4)
          "H" (swap! canvas horizontal-paint a1 a2 a3 a4)
          "F" (swap! canvas fill a1 a2 a3)
          "S" (println (show @canvas))
          "X" (error "Bye!")
          ""  (do)
          (error "ERROR: Unknown command (" line ")"))
        (catch java.lang.AssertionError e
            (error "ERROR: " (.getMessage e))))
      (when-not (= cmd "X")
        (recur (prompt) canvas)))))
