(ns bitmap-editor.validations)

(defn positive-integer? [n]
  (and (integer? n)
       (pos? n)))

(defn x-in-bounds? [img x]
  (let [cols (count (first img))]
    (<= x cols)))

(defn y-in-bounds? [img y]
  (let [rows (count img)]
    (<= y rows)))

(defn colour? [c]
  (and (not (nil? c))
       (string? c)
       (re-matches #"^[A-Z]$" c)))

(defn canvas-ready? [img]
  (not (empty? img)))
